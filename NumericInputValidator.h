#ifndef NUMERICINPUTVALIDATOR_H
#define NUMERICINPUTVALIDATOR_H

#include <string>

class NumericInputValidator
{
    uint minRange, maxRange;
public:
    NumericInputValidator();
    NumericInputValidator(uint min, uint max);
    ~NumericInputValidator();
    bool IsValidInput(std::string input);

};

#endif // NUMERICINPUTVALIDATOR_H
