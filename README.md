Trimble
=======

Prerequsites
------------

### UnitTest++ ###

Download and install latest version from [UnitTest-cpp](https://github.com/unittest-cpp/unittest-cpp)

Build Steps
-----------

To build the main program run the following make command
```
make -f Trimble.mk
```

Output will be put in a "Debug" directory. Run the application "Trimble" from there.

To build the test suite, navigate to "Trimble-tests" and run:
```
make -f Trimble-test.mk
```

The test runner will be under "Trimble-tests/Debug"
