#ifndef BIGNUMDECIMALENCODING_H
#define BIGNUMDECIMALENCODING_H

#include <vector>
#include <stdint.h>
#include <string>

class BigNumDecimalEncoding
{
private:
    std::vector<uint16_t> _array;
public:
    BigNumDecimalEncoding();
    BigNumDecimalEncoding(std::string input);
    BigNumDecimalEncoding(int input);
    BigNumDecimalEncoding(std::vector<uint16_t> input);
    ~BigNumDecimalEncoding();
    
    BigNumDecimalEncoding operator*(const BigNumDecimalEncoding& other);
    uint16_t& operator[](size_t pos);
    const uint16_t& operator[](size_t pos) const;
    
    BigNumDecimalEncoding Power(uint power);
    uint length() const;
    std::string ToString();
};

#endif // BIGNUMDECIMALENCODING_H
