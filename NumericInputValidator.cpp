#include "NumericInputValidator.h"
#include <climits>
#include <string>

NumericInputValidator::NumericInputValidator()
{
    minRange = 0;
    maxRange = UINT_MAX;
}

NumericInputValidator::~NumericInputValidator()
{
}

bool NumericInputValidator::IsValidInput(std::string input)
{
    if(input.length() == 0) return false;
    for(size_t i = 0; i < input.length(); i++)
    {
        if(input[i] < '0' || input[i] > '9')
        {
            return false;
        }
    }
    try
    {
        ulong testNum = std::stoul(input);
        if(testNum < minRange || testNum > maxRange)
            return false;
    }
    catch (const std::exception& e)
    {
        return false;
    }
    return true;
}

NumericInputValidator::NumericInputValidator(uint min, uint max)
{
    minRange = min;
    maxRange = max;
}
