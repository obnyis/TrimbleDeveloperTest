#include "BigNumDecimalEncoding.h"
#include <stdlib.h>
#include <sstream>
#include <utility>
#include <iostream>

BigNumDecimalEncoding::BigNumDecimalEncoding()
{
    _array.push_back(0);
}

BigNumDecimalEncoding::~BigNumDecimalEncoding()
{
}

BigNumDecimalEncoding::BigNumDecimalEncoding(std::string input)
{
    for(auto it = input.rbegin(); it != input.rend(); ++it)
    {
        if(*it == '\r' || *it == '\n') continue;
        _array.push_back(*it - '0');
    }
}

BigNumDecimalEncoding::BigNumDecimalEncoding(int input)
{
    std::stringstream buf;
    buf << input;
    std::string t = buf.str();
    for(auto it = t.rbegin(); it != t.rend(); ++it)
    {
        _array.push_back(*it - '0');
    }
}

/**
 * @brief Constructs a bignum from a vector of uint16, performing carries on
 * any value larger than 9
 * @param input
 * @return 
 */
BigNumDecimalEncoding::BigNumDecimalEncoding(std::vector<uint16_t> input)
{
    input.push_back(0);
    input.push_back(0);
    for(size_t i = 0; i < input.size(); i++)
    {
        int n = input[i];
        int j = 1;
        _array.push_back(n%10);
        n /= 10;
        while(n > 0)
        {
            input[i+j] += n%10;
            j++;
            n /= 10;
        }
    }
    for(auto it = _array.rbegin(); it != _array.rend() - 1; ++it)
    {
        if(*it != 0) break;
        _array.pop_back();
    }
}

/**
 * @brief Performs long multiplication on two BigNum's
 * @param other
 * @return 
 */
BigNumDecimalEncoding BigNumDecimalEncoding::operator *(const BigNumDecimalEncoding& other)
{
    uint16_t longmult[this->length()][other.length()];
    std::vector<uint16_t> sum;
    for(uint k = 0; k < this->length(); k++)
    {
        for(uint l = 0; l < other.length(); l++)
        {
            longmult[k][l] = (*this)[k] * other[l];
        }
    }
    for(int k = 0; k < (int)(this->length() + other.length()-1); k++)
    {
        uint16_t tmp = 0;
        for(int l = 0; l < (int)other.length(); l++)
        {
            if(k-l >= 0 && k-l < (int)this->length())
                tmp += longmult[k-l][l];
        }
        sum.push_back(tmp);
    }
    return BigNumDecimalEncoding(sum);
}


std::string BigNumDecimalEncoding::ToString()
{
    std::stringstream buf;
    for(auto it = _array.rbegin(); it != _array.rend(); ++it)
    {
        buf << *it;
    }
    return buf.str();
}


uint BigNumDecimalEncoding::length() const
{
    return _array.size();
}


uint16_t& BigNumDecimalEncoding::operator [](size_t pos)
{
    return _array[pos];
}

const uint16_t& BigNumDecimalEncoding::operator [](size_t pos) const
{
    return _array[pos];
}

BigNumDecimalEncoding BigNumDecimalEncoding::Power(uint power)
{
    BigNumDecimalEncoding number = BigNumDecimalEncoding(1);
    for(uint i = 0; i < power; i++)
    {
        number = number * (*this);
    }
    return number;
}
