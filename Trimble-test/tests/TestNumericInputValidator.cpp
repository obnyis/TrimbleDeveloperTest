#include <UnitTest++.h>
#include "NumericInputValidator.h"

namespace
{
    TEST(ValidInputs)
    {
        NumericInputValidator n = NumericInputValidator();
        CHECK_EQUAL(true, n.IsValidInput("1"));
        CHECK_EQUAL(true, n.IsValidInput("10"));
        CHECK_EQUAL(true, n.IsValidInput("0"));
        CHECK_EQUAL(false, n.IsValidInput("123456789012345678901234567890"));
    }
    
    TEST(Range)
    {
        NumericInputValidator n(0,1);
        CHECK_EQUAL(true, n.IsValidInput("0"));
        CHECK_EQUAL(true, n.IsValidInput("1"));
        CHECK_EQUAL(false, n.IsValidInput("2"));
        NumericInputValidator n2(0,99999);
        CHECK_EQUAL(true, n2.IsValidInput("0"));
        CHECK_EQUAL(true, n2.IsValidInput("99999"));
        CHECK_EQUAL(false, n2.IsValidInput("100000"));
    }
    
    TEST(EmptyInput)
    {
        NumericInputValidator n = NumericInputValidator();
        CHECK_EQUAL(false, n.IsValidInput(""));
        CHECK_EQUAL(false, n.IsValidInput(" "));
        CHECK_EQUAL(false, n.IsValidInput("\r"));
        CHECK_EQUAL(false, n.IsValidInput("\n"));
        CHECK_EQUAL(false, n.IsValidInput("\r\n"));
    }
    
    TEST(FloatInputs)
    {
        NumericInputValidator n = NumericInputValidator();
        CHECK_EQUAL(false, n.IsValidInput("1.0"));
        CHECK_EQUAL(false, n.IsValidInput("999.9999999999999999999999"));
        CHECK_EQUAL(false, n.IsValidInput("-10.213"));
    }
    
    TEST(NegativeInputs)
    {
        NumericInputValidator n = NumericInputValidator();
        CHECK_EQUAL(false, n.IsValidInput("-1"));
        CHECK_EQUAL(false, n.IsValidInput("-10"));
        CHECK_EQUAL(false, n.IsValidInput("-0"));
        CHECK_EQUAL(false, n.IsValidInput("-123456789012345678901234567890"));
    }
    
    TEST(CharInputs)
    {
        NumericInputValidator n = NumericInputValidator();
        CHECK_EQUAL(false, n.IsValidInput("a"));
        CHECK_EQUAL(false, n.IsValidInput("*"));
        CHECK_EQUAL(false, n.IsValidInput("qwertyuiop"));
        CHECK_EQUAL(false, n.IsValidInput("abc123456789012345678901234567890"));
        
    }
}
