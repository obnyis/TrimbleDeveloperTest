#include <UnitTest++.h>
#include "BigNumDecimalEncoding.h"

namespace
{
    TEST(TestConstructor)
    {
        CHECK_EQUAL("0", BigNumDecimalEncoding().ToString());
        CHECK_EQUAL("111", BigNumDecimalEncoding(111).ToString());
        CHECK_EQUAL("234", BigNumDecimalEncoding(234).ToString());
        CHECK_EQUAL("99999", BigNumDecimalEncoding(99999).ToString());
        CHECK_EQUAL("111", BigNumDecimalEncoding("111").ToString());
        CHECK_EQUAL("234", BigNumDecimalEncoding("234").ToString());
        CHECK_EQUAL("99999", BigNumDecimalEncoding("99999").ToString());
    }
    
    TEST(TestConstructorWithNewline)
    {
        CHECK_EQUAL("111", BigNumDecimalEncoding("111\r").ToString());
        CHECK_EQUAL("234", BigNumDecimalEncoding("234\n").ToString());
        CHECK_EQUAL("99999", BigNumDecimalEncoding("99999\r\n").ToString());
    }
    
    TEST(TestMultiply)
    {
        CHECK_EQUAL("0", (BigNumDecimalEncoding("1")*BigNumDecimalEncoding("0")).ToString());
        CHECK_EQUAL("1", (BigNumDecimalEncoding("1")*BigNumDecimalEncoding("1")).ToString());
        CHECK_EQUAL("9", (BigNumDecimalEncoding("3")*BigNumDecimalEncoding("3")).ToString());
        CHECK_EQUAL("81", (BigNumDecimalEncoding("9")*BigNumDecimalEncoding("9")).ToString());
        CHECK_EQUAL("11", (BigNumDecimalEncoding("11")*BigNumDecimalEncoding("1")).ToString());
        CHECK_EQUAL("121", (BigNumDecimalEncoding("11")*BigNumDecimalEncoding("11")).ToString());
        CHECK_EQUAL("9801", (BigNumDecimalEncoding("99")*BigNumDecimalEncoding("99")).ToString());
    }
    
    TEST(TestPower)
    {
        CHECK_EQUAL("1", (BigNumDecimalEncoding("1").Power(0)).ToString());
        CHECK_EQUAL("0", (BigNumDecimalEncoding("0").Power(1)).ToString());
        CHECK_EQUAL("1", (BigNumDecimalEncoding("1").Power(1)).ToString());
        CHECK_EQUAL("9", (BigNumDecimalEncoding("3").Power(2)).ToString());
        CHECK_EQUAL("387420489", (BigNumDecimalEncoding("9").Power(9)).ToString());
        CHECK_EQUAL(
            "72367033806371673149109894141163778628811792657571658906010558390395870363798401744095280686155507736404921657070284961721828960592977909542637098897697223102622628566787654091327825453991595140205701412961364188732408936197890553699715836951569999800431957769217006743321026257517932764164662319487914962533302741368207211189494615326552790667720411285474162636765168907211924134973374304496019635376665858559941735703924836467756917247995469583487467791524582153744522107597865277798136080074161485280424274076931083994487111719562249702540362855712911132265966235754355353516703339043001506118520760359577737869472018617942120590873170710805078696371738906375721785723",
            (BigNumDecimalEncoding("123").Power(321)).ToString());
    }
}
