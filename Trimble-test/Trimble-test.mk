##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=Trimble-test
ConfigurationName      :=Debug
WorkspacePath          :=/home/noah/CodeLiteWorkspace
ProjectPath            :=/media/sf_GitLab/TrimbleDeveloperTest/Trimble-test
IntermediateDirectory  :=Debug
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=Noah
Date                   :=04/12/17
CodeLitePath           :=/home/noah/.codelite
LinkerName             :=/usr/bin/g++
SharedObjectLinkerName :=/usr/bin/g++ -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=$(IntermediateDirectory)/$(ProjectName)
Preprocessors          :=
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :="Trimble-test.txt"
PCHCompileFlags        :=
MakeDirCommand         :=mkdir -p
LinkOptions            :=  
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch). $(IncludeSwitch).. $(IncludeSwitch)/usr/local/include/UnitTest++ 
IncludePCH             := 
RcIncludePath          := 
Libs                   := $(LibrarySwitch)UnitTest++ 
ArLibs                 :=  "libUnitTest++.a" 
LibPath                := $(LibraryPathSwitch). $(LibraryPathSwitch)../Debug 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := /usr/bin/ar rcu
CXX      := /usr/bin/g++
CC       := /usr/bin/gcc
CXXFLAGS :=  -g -std=c++14 -std=c++11 $(Preprocessors)
CFLAGS   :=  -g $(Preprocessors)
ASFLAGS  := 
AS       := /usr/bin/as


##
## User defined environment variables
##
CodeLiteDir:=/usr/share/codelite
Objects0=$(IntermediateDirectory)/main.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_BigNumDecimalEncoding.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_NumericInputValidator.cpp$(ObjectSuffix) $(IntermediateDirectory)/tests_TestNumericInputValidator.cpp$(ObjectSuffix) $(IntermediateDirectory)/tests_TestBigNumDecimalEncoding.cpp$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d "../../../../home/noah/CodeLiteWorkspace/.build-debug/Trimble" $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

"../../../../home/noah/CodeLiteWorkspace/.build-debug/Trimble":
	@$(MakeDirCommand) "../../../../home/noah/CodeLiteWorkspace/.build-debug"
	@echo stam > "../../../../home/noah/CodeLiteWorkspace/.build-debug/Trimble"




MakeIntermediateDirs:
	@test -d Debug || $(MakeDirCommand) Debug


$(IntermediateDirectory)/.d:
	@test -d Debug || $(MakeDirCommand) Debug

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/main.cpp$(ObjectSuffix): main.cpp $(IntermediateDirectory)/main.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/media/sf_GitLab/TrimbleDeveloperTest/Trimble-test/main.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/main.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/main.cpp$(DependSuffix): main.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/main.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/main.cpp$(DependSuffix) -MM main.cpp

$(IntermediateDirectory)/main.cpp$(PreprocessSuffix): main.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/main.cpp$(PreprocessSuffix) main.cpp

$(IntermediateDirectory)/up_BigNumDecimalEncoding.cpp$(ObjectSuffix): ../BigNumDecimalEncoding.cpp $(IntermediateDirectory)/up_BigNumDecimalEncoding.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/media/sf_GitLab/TrimbleDeveloperTest/BigNumDecimalEncoding.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_BigNumDecimalEncoding.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_BigNumDecimalEncoding.cpp$(DependSuffix): ../BigNumDecimalEncoding.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_BigNumDecimalEncoding.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_BigNumDecimalEncoding.cpp$(DependSuffix) -MM ../BigNumDecimalEncoding.cpp

$(IntermediateDirectory)/up_BigNumDecimalEncoding.cpp$(PreprocessSuffix): ../BigNumDecimalEncoding.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_BigNumDecimalEncoding.cpp$(PreprocessSuffix) ../BigNumDecimalEncoding.cpp

$(IntermediateDirectory)/up_NumericInputValidator.cpp$(ObjectSuffix): ../NumericInputValidator.cpp $(IntermediateDirectory)/up_NumericInputValidator.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/media/sf_GitLab/TrimbleDeveloperTest/NumericInputValidator.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_NumericInputValidator.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_NumericInputValidator.cpp$(DependSuffix): ../NumericInputValidator.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_NumericInputValidator.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_NumericInputValidator.cpp$(DependSuffix) -MM ../NumericInputValidator.cpp

$(IntermediateDirectory)/up_NumericInputValidator.cpp$(PreprocessSuffix): ../NumericInputValidator.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_NumericInputValidator.cpp$(PreprocessSuffix) ../NumericInputValidator.cpp

$(IntermediateDirectory)/tests_TestNumericInputValidator.cpp$(ObjectSuffix): tests/TestNumericInputValidator.cpp $(IntermediateDirectory)/tests_TestNumericInputValidator.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/media/sf_GitLab/TrimbleDeveloperTest/Trimble-test/tests/TestNumericInputValidator.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/tests_TestNumericInputValidator.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/tests_TestNumericInputValidator.cpp$(DependSuffix): tests/TestNumericInputValidator.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/tests_TestNumericInputValidator.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/tests_TestNumericInputValidator.cpp$(DependSuffix) -MM tests/TestNumericInputValidator.cpp

$(IntermediateDirectory)/tests_TestNumericInputValidator.cpp$(PreprocessSuffix): tests/TestNumericInputValidator.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/tests_TestNumericInputValidator.cpp$(PreprocessSuffix) tests/TestNumericInputValidator.cpp

$(IntermediateDirectory)/tests_TestBigNumDecimalEncoding.cpp$(ObjectSuffix): tests/TestBigNumDecimalEncoding.cpp $(IntermediateDirectory)/tests_TestBigNumDecimalEncoding.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/media/sf_GitLab/TrimbleDeveloperTest/Trimble-test/tests/TestBigNumDecimalEncoding.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/tests_TestBigNumDecimalEncoding.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/tests_TestBigNumDecimalEncoding.cpp$(DependSuffix): tests/TestBigNumDecimalEncoding.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/tests_TestBigNumDecimalEncoding.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/tests_TestBigNumDecimalEncoding.cpp$(DependSuffix) -MM tests/TestBigNumDecimalEncoding.cpp

$(IntermediateDirectory)/tests_TestBigNumDecimalEncoding.cpp$(PreprocessSuffix): tests/TestBigNumDecimalEncoding.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/tests_TestBigNumDecimalEncoding.cpp$(PreprocessSuffix) tests/TestBigNumDecimalEncoding.cpp


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r Debug/


