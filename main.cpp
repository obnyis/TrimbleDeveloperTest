#include <stdio.h>
#include <iostream>
#include <algorithm>
#include <string>
#include "NumericInputValidator.h"
#include "BigNumDecimalEncoding.h"

int main(int argc, char **argv)
{
    std::string buffer;
    std::string reverse;
    NumericInputValidator validator(0,99999);
    do
    {
        std::cout << "Enter a number (0-99999): ";
        std::getline(std::cin, buffer);
    } while(!validator.IsValidInput(buffer));
    
    BigNumDecimalEncoding base = BigNumDecimalEncoding(buffer);
    reverse.resize(buffer.size());
    std::reverse_copy(buffer.begin(), buffer.end(), reverse.begin());
    
    std::cout << "Result: " << buffer << "^" << reverse << " is " << base.Power(stoi(reverse)).ToString() << std::endl;
	return 0;
}
