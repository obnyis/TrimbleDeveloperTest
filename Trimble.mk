##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=Trimble
ConfigurationName      :=Debug
WorkspacePath          :=/home/noah/CodeLiteWorkspace
ProjectPath            :=/media/sf_GitLab/TrimbleDeveloperTest
IntermediateDirectory  :=./Debug
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=Noah
Date                   :=04/12/17
CodeLitePath           :=/home/noah/.codelite
LinkerName             :=/usr/bin/g++
SharedObjectLinkerName :=/usr/bin/g++ -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=$(IntermediateDirectory)/$(ProjectName)
Preprocessors          :=
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :="Trimble.txt"
PCHCompileFlags        :=
MakeDirCommand         :=mkdir -p
LinkOptions            :=  
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch). 
IncludePCH             := 
RcIncludePath          := 
Libs                   := 
ArLibs                 :=  
LibPath                := $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := /usr/bin/ar rcu
CXX      := /usr/bin/g++
CC       := /usr/bin/gcc
CXXFLAGS :=  -g -O0 -std=c++14 -std=c++11 -Wall $(Preprocessors)
CFLAGS   :=  -g -O0 -Wall $(Preprocessors)
ASFLAGS  := 
AS       := /usr/bin/as


##
## User defined environment variables
##
CodeLiteDir:=/usr/share/codelite
Objects0=$(IntermediateDirectory)/main.cpp$(ObjectSuffix) $(IntermediateDirectory)/BigNumDecimalEncoding.cpp$(ObjectSuffix) $(IntermediateDirectory)/NumericInputValidator.cpp$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

MakeIntermediateDirs:
	@test -d ./Debug || $(MakeDirCommand) ./Debug


$(IntermediateDirectory)/.d:
	@test -d ./Debug || $(MakeDirCommand) ./Debug

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/main.cpp$(ObjectSuffix): main.cpp $(IntermediateDirectory)/main.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/media/sf_GitLab/TrimbleDeveloperTest/main.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/main.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/main.cpp$(DependSuffix): main.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/main.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/main.cpp$(DependSuffix) -MM main.cpp

$(IntermediateDirectory)/main.cpp$(PreprocessSuffix): main.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/main.cpp$(PreprocessSuffix) main.cpp

$(IntermediateDirectory)/BigNumDecimalEncoding.cpp$(ObjectSuffix): BigNumDecimalEncoding.cpp $(IntermediateDirectory)/BigNumDecimalEncoding.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/media/sf_GitLab/TrimbleDeveloperTest/BigNumDecimalEncoding.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/BigNumDecimalEncoding.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/BigNumDecimalEncoding.cpp$(DependSuffix): BigNumDecimalEncoding.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/BigNumDecimalEncoding.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/BigNumDecimalEncoding.cpp$(DependSuffix) -MM BigNumDecimalEncoding.cpp

$(IntermediateDirectory)/BigNumDecimalEncoding.cpp$(PreprocessSuffix): BigNumDecimalEncoding.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/BigNumDecimalEncoding.cpp$(PreprocessSuffix) BigNumDecimalEncoding.cpp

$(IntermediateDirectory)/NumericInputValidator.cpp$(ObjectSuffix): NumericInputValidator.cpp $(IntermediateDirectory)/NumericInputValidator.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/media/sf_GitLab/TrimbleDeveloperTest/NumericInputValidator.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/NumericInputValidator.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/NumericInputValidator.cpp$(DependSuffix): NumericInputValidator.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/NumericInputValidator.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/NumericInputValidator.cpp$(DependSuffix) -MM NumericInputValidator.cpp

$(IntermediateDirectory)/NumericInputValidator.cpp$(PreprocessSuffix): NumericInputValidator.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/NumericInputValidator.cpp$(PreprocessSuffix) NumericInputValidator.cpp


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r ./Debug/


